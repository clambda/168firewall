A project for this class: http://inst.eecs.berkeley.edu/~cs168/fa14/

A skeleton was provided. firewall.py written by Nathan Chong and Joel Terry.  For full documentation, refer to spec.pdf

Nathan Chong - cs168-cc
Joel Terry - cs168-no

We liked this project. It felt more real since we dealt with actual packets sent and received while using the internet.  We would only have asked that we got an example packet to see exactly what format these packets were in.  It took us a while to figure out that we should be indexing the string by bytes.