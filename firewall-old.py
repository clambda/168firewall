#!/usr/bin/env python

from main import PKT_DIR_INCOMING, PKT_DIR_OUTGOING
import struct

# TODO: Feel free to import any Python standard moduless as necessary.
# (http://docs.python.org/2/library/)
# You must NOT use any 3rd-party libraries, though.


class Firewall:
    def __init__(self, config, iface_int, iface_ext):
        self.iface_int = iface_int
        self.iface_ext = iface_ext
        self.rules = []
        self.geo = GeoDB()

        #TODO: Load the firewall rules (from rule_filename) here.
        #ruleFile = open(config['rule'])
        ruleFile = open(config['rule'])
        for line in ruleFile:
            line = line.lower()
            args = line.split()
            if len(args) == 4:
                args.append(self.geo)
                self.rules.append(ProtocolRule(*args))
            elif len(args) == 3:
                self.rules.append(DNSRule(args[0], args[2]))
        

        # TODO: Load the GeoIP DB ('geoipdb.txt') as well.
        geoFile = open('geoipdb.txt')
        for line in geoFile:
            line = line.lower()
            args = line.split()
            self.geo.add(*args)

    # @pkt_dir: either PKT_DIR_INCOMING or PKT_DIR_OUTGOING
    # @pkt: the actual data of the IPv4 packet (including IP header)
    def handle_packet(self, pkt_dir, pkt):
        # TODO: Your main firewall code will be here.
        packet = Packet(pkt, pkt_dir)
        if packet.verdict != "pass":
            return
        for rule in self.rules:
            if rule.match(packet):
                packet.verdict = rule.verdict
        if packet.verdict == "deny":
            packet.verdict = "pass"
            pkt_dir = 0 if pkt_dir == 1 else 1
            if packet.protocol == "udp":
                if packet.qtype == 28:
                    return
                pkt = makeDNSResponse(pkt, packet)
            else:
                pkt = makeRST(pkt)
        if packet.verdict != "pass":
            return
        #check if http
        if packet.protocol == "tcp" and packet.port == 80:
            ip_length = (unpackBytes(pkt[0], 1) % 16) * 4
            tcp_length = msb(unpackBytes(pkt[ip_length + 12], 1), 4) * 4
            http = pkt[ip_length + tcp_length:]
            process_http_packet(packet, http)
        # If we get here, we pass the packet
        if pkt_dir == 0: # If the packet is incoming
            #print("PASSED INCOMING PACKET")
            self.iface_int.send_ip_packet(pkt)
        else:
            #print("PASSED OUTGOING PACKET")
            self.iface_ext.send_ip_packet(pkt)

class GeoDB:
    def __init__(self):
        self.countries = {}

    def add(self, minIP, maxIP, cn):
        low = addressToInteger(minIP)
        high = addressToInteger(maxIP)
        if cn in self.countries:
            self.countries[cn].append((low, high))
        else:
            self.countries[cn] = [(low, high)]

    def find(self, num, cn):
        if cn not in self.countries:
            return False
        lst = self.countries[cn]
        low, high = 0, len(lst) - 1
        while low <= high:
            mid = (low + high) / 2
            curr = lst[mid]
            if num < curr[0]:
                high = mid - 1
            elif num > curr[1]:
                low = mid + 1
            else:
                return True
        return False


### deny tcp / rst ###

class IPHeader:
    #@param header is only the first 20 bytes of the ip packet
    def __init__(self, header):
        self.length = (unpackBytes(header[0], 1) % 16) * 4
        self.source = header[12:16] #don't need to unpack source/dest
        self.dest = header[16:20]
        self.id = unpackBytes(header[4:6], 2)

class TCPHeader:
    def __init__(self, header):
        self.sourcePort = header[0:2]
        self.destPort = header[2:4]
        self.sequence = unpackBytes(header[4:8], 4)
        self.length = (unpackBytes(header[12], 1) // 16) * 4


def makeRST(pkt):
    ipHeader = IPHeader(pkt[0:20])
    tcpStart = ipHeader.length
    tcpHeader = TCPHeader(pkt[tcpStart:tcpStart+20])
    return makeDenyIPHeader(ipHeader, "tcp")[1] + makeDenyTCPHeader(tcpHeader, ipHeader)

def makeDNSResponse(pkt, packet):
    ipHeader = IPHeader(pkt[0:20])
    udpStart = ipHeader.length
    source_port = pkt[udpStart + 2:udpStart + 4]
    dest_port = pkt[udpStart:udpStart + 2]
    (checksum, ip) = makeDenyIPHeader(ipHeader, "udp")
    dnsStart = udpStart + 8
    dns = makeDenyDNS(pkt[dnsStart:], packet.answerStart - dnsStart) #make DNS response from start of DNS header
    udp = source_port + dest_port
    udp += struct.pack("!H", len(dns) + 8) + struct.pack("!H", 0)
    checksum += len(dns) + 28
    checksum = calculateChecksum(checksum)
    ip = ip[0:2] + struct.pack("!H", len(dns) + 28) + ip[4:10] + checksum + ip[12:]
    #IP total length and checksum change
    return ip + udp + dns

#hard-coding the start of our IP header for deny packets
word1 = struct.pack('!L', 0x45000028)
word2 = struct.pack('!L', 0x00000000)
ttl = struct.pack('!B', 0x40)
default_deny_ip_header = word1 + word2 + ttl


def makeDenyDNS(pkt, answerStart):
    header = pkt[0:2] #copy ID of request
    #checksum = unpackBytes(header, 2)
    current = unpackBytes(pkt[2], 1) #copy QR, opcode, AA, TC, RD
    current = (current | 0x84) #make sure QR and AA are set to 1
    #checksum += current
    header += struct.pack("!B", current)
    #current = unpackBytes(pkt[3], 1)
    #checksum += current
    current = unpackBytes(pkt[3], 1)
    current = (current | 0x30)
    header += struct.pack("!B", current) #copy RA, Z, Rcode from response
    #checksum += 2 #QDCount, ANCount both 1
    one = struct.pack("!H", 1)
    header += one + one
    header += struct.pack("!L", 0) #NSCount, ARCount both 0
    question = pkt[12:answerStart] #copy the questionsection
    #i = 0
    #while i < len(question)-4:
    #    checksum += 2 * unpackBytes(question[i:i+2], 2) #Add twice as QName shows up again later
    #    i += 2
    #checksum += unpackBytes(question[i:i+2], 2) #Qtype
    #checksum += unpackBytes(question[i+2:i+4], 2) #Qclass
    answer = pkt[12:answerStart-4] #Name for answer section is also Qname
    answer += one + one #Type A: a host address, Class 1: the Internet
    answer += struct.pack("!L", 1) #TTL = 1 second
    #checksum += 3
    answer += struct.pack("!H", 4) #RData is 4 bytes long (an IP address)
    #checksum += 4
    answer += struct.pack("!L", 917364886) #I hope this is the right IP
    #checksum += 0x36AD + 0xE096
    return header + question + answer



def makeDenyIPHeader(ipHeader, protocol):
    checksum = 0x4500 + 0x0028
    if protocol == "tcp":
        protocol = struct.pack('!B', 0x06)
        checksum += 0x4006
    else:
        protocol = struct.pack('!B', 0x11)
        checksum += 0x4011
    source = ipHeader.dest
    dest = ipHeader.source
    checksum += unpackBytes(source[0:2], 2) + unpackBytes(source[2:4], 2)
    checksum += unpackBytes(dest[0:2], 2) + unpackBytes(dest[2:4], 2)
    precheck = checksum - 0x0028
    checksum = calculateChecksum(checksum)
    return (precheck, default_deny_ip_header + protocol + checksum + source + dest)


def makeDenyTCPHeader(tcpHeader, ipHeader):
    sourceIP = ipHeader.dest
    destIP = ipHeader.source
    checksum = unpackBytes(sourceIP[0:2], 2) + unpackBytes(sourceIP[2:4], 2)
    checksum += unpackBytes(destIP[0:2], 2) + unpackBytes(destIP[2:4], 2)
    checksum += 0x0006 + 0x0014 #protocol and tcplength
    source = tcpHeader.destPort
    dest = tcpHeader.sourcePort
    checksum += unpackBytes(source, 2)
    checksum += unpackBytes(dest, 2)
    sequence = struct.pack('!L', 0)
    ack = tcpHeader.sequence + 1
    checksum += lsb(ack, 16)
    checksum += msb(ack, 16)
    ack = struct.pack('!L', ack)
    checksum += 0x5014
    word4 = struct.pack('!L', 0x50140000)
    checksum = calculateChecksum(checksum)
    urgent = struct.pack('!H', 0)
    return source + dest + sequence + ack + word4 + checksum + urgent
    


def calculateChecksum(n):
    if n > 0xFFFF:
        n = lsb(n, 16) + msb(n, 16)
    return struct.pack('!H', n ^ 0xFFFF)



###
    





        

class Packet:
    def __init__(self, pkt, direction):
        #print("GOT PACKET")
        self.verdict = "pass" #we pass the packet by default
        if len(pkt) < 20: #in case packet is wrong, we dont want indexing errors
            self.verdict = "drop"
            return
        headlength = unpackBytes(pkt[0], 1) % 16 #length of ip header, as int
        nextheader = headlength * 4
        protocol = unpackBytes(pkt[9], 1) #protocol field of header, as int
        if len(pkt) < nextheader + 6:
            self.verdict = "drop"
            return
        #Set protocol and external port
        self.setProtocol(pkt, protocol, nextheader, direction)
        if self.verdict != "pass":
            return
        #print("PROTOCOL: " + self.protocol)
        #print("EXTERNAL PORT: " + str(self.port))
        #Set external ip
        self.setExternalIp(pkt, direction)
        #print("EXTERNAL IP: " + str(self.ip))
        #Lastly, check if we have a dns packet
        self.dns = self.isdns(pkt, nextheader, direction)
        #print("DNS RULES APPLY: " + str(self.dns))

    """ Sets this packets protocol and port # given the ipv4 protocol header field """
    def setProtocol(self, pkt, protocol, nextheader, direction):
        if protocol == 1:
            self.protocol = "icmp"
            self.port = unpackBytes(pkt[nextheader], 1) #icmp type header field
        elif protocol == 6:
            self.protocol = "tcp"
            self.seqno = unpackBytes(pkt[nextheader + 4:nextheader + 8], 4)
            if direction == 0: #packet is incoming
                self.port = unpackBytes(pkt[nextheader:nextheader + 2], 2)
                self.internal_port = unpackBytes(pkt[nextheader + 2:nextheader + 4], 2)
            else:
                self.port = unpackBytes(pkt[nextheader + 2:nextheader + 4], 2)
                self.internal_port = unpackBytes(pkt[nextheader:nextheader + 2], 2)
        elif protocol == 17:
            self.protocol = "udp"
            if len(pkt) != unpackBytes(pkt[nextheader + 4:nextheader + 6], 2) + nextheader:
                self.verdict = "drop"
                return
            if direction == 0: #packet is incoming
                self.port = unpackBytes(pkt[nextheader:nextheader + 2], 2)
            else:
                self.port = unpackBytes(pkt[nextheader + 2:nextheader + 4], 2)
        else:
            self.port = 0
            self.protocol = "other"

    """Sets external IP of packet given its direction. """
    def setExternalIp(self, pkt, direction):
        if direction == 0: #packet is incoming
            self.ip = unpackBytes(pkt[12:16], 4) #source ip header field
        else: #packet is outgoing
            self.ip = unpackBytes(pkt[16:20], 4) #destination ip header field

    """ Returns whether or not this packet is a DNS query, given the start of the header after IP."""
    def isdns(self, pkt, nextheader, direction):
        if direction == 1 and self.protocol == "udp" and self.port == 53:
            dnsheader = nextheader + 8 #the start of the dns header (after udp)
            if len(pkt) < dnsheader + 16: #make sure pkt has appropriate indexes
                return False
            qdcount = unpackBytes(pkt[dnsheader + 4:dnsheader + 6], 2)
            question = dnsheader + 12 #start of question section after dns header
            qname = ""
            label = unpackBytes(pkt[question], 1)
            current = question + 1 #current position in question section
            while label != 0:
                for i in range(0, label):
                    qname += str(unichr(unpackBytes(pkt[current], 1)))
                    current += 1
                label = unpackBytes(pkt[current], 1)
                current += 1
                if label != 0:
                    qname += "."
            qtype = unpackBytes(pkt[current:current + 2], 2)
            qclass = unpackBytes(pkt[current + 2: current + 4], 2)
            if qdcount == 1 and (qtype == 1 or qtype == 28) and qclass == 1:
                self.url = qname
                self.qtype = qtype
                self.answerStart = current + 4
                return True
        return False

"""Returns pkt as a binary string.  NOT USED. """
def toBinary(pkt):
    packet = ""
    i = 0
    while i + 4 < len(pkt):
        bytes = struct.unpack('!B', pkt[i:i + 4])[0]
        packet += bin(bytes)[2:]
        i += 4
    return packet

"""Returns n bytes of bytes as an int given bytes is in Big Endian."""
def unpackBytes(bytes, n):
    if n == 1:
        return struct.unpack('!B', bytes)[0]
    if n == 2:
        return struct.unpack('!H', bytes)[0]
    if n == 4:
        return struct.unpack('!L', bytes)[0]
    #But we should never need to unpack something other than 1, 2 or 4 bytes
    i = 0
    while i + 4 < len(bytes):
        byte = struct.unpack('!B', bytes[i:i + 4])[0]
        data += bin(bytes)[2:]
        i += 4
    return int(data, 2)






#address and port are stored as a range, even if they're single values
class ProtocolRule:
    def __init__(self, verdict, protocol, address, port, geo):
        self.geo = geo
        self.verdict = verdict
        self.protocol = protocol

        #address range
        if len(address) == 2: #country code
            self.isCN = True
            self.address = address
        else:
            self.isCN = False
            if address == "any":
                prefix, size = "0.0.0.0", 0
            elif "/" in address:
                prefix, size = address.split("/")
            else:
                prefix, size = address, 32
            self.address = prefixToRange(prefix, size)

        #port range
        if port == "any":
            self.port = (0, float("inf"))
        elif "-" in port:
            self.port = tuple([int(e) for e in port.split("-")])
        else:
            self.port = (int(port), int(port))

    def match(self, packet):
        if packet.protocol != self.protocol:
            return False
        if not inRange(packet.port, self.port):
            return False
        if self.isCN:
            matchIP = self.geo.find(packet.ip, self.address)
        else:
            matchIP = inRange(packet.ip, self.address)
        return matchIP
    


class DNSRule:
    def __init__(self, verdict, domain):
        self.verdict = verdict.lower()
        self.domain = domain.lower()

    def match(self, packet):
        if not packet.dns:
            return False
        if self.domain[0] == "*":
            exact = self.domain[1:]
            i = len(packet.url) - len(exact)
            if i < 0: return False
            return packet.url[i:] == exact
        return packet.url == self.domain


###HTTP###

httplog = open('http.log', 'a')

streams = []

def find_stream(internal_port, external_ip):
    for s in streams:
        if s.internal_port == internal_port and s.external_ip == external_ip:
            return s
    return None


#should only be called on tcp packets with external port 80
def process_http_packet(packet, http):
    seqno = packet.seqno
    internal_port = packet.internal_port
    external_ip = packet.ip
    stream = find_stream(internal_port, external_ip)
    if stream:
        if seqno > stream.expected_seqno:
            packet.verdict = "drop"
            return
        elif seqno < stream.expected_seqno:
            return
        else:
            stream.build(http)
            stream.expected_seqno += 1
    else: #i don't think i'm accounting for loss of first packet
        stream = httpStream(internal_port, external_ip, seqno + 1)
        streams.append(stream)
        stream.build(http)



class httpStream:
    def __init__(self, internal_port, external_ip, seqno):
        self.expected_seqno = seqno
        self.internal_port = internal_port
        self.external_ip = external_ip
        self.request_frags = []
        self.response_frags = []
        self.requestInfo = None
        self.responseInfo = None

    def build(self, fragment):
        if not self.requestInfo:
            self.build_request(fragment)
        else:
            self.build_response(fragment)

    def build_request(self, fragment):
        end = fragment.find("\r\n\r\n")
        if end > -1:
            self.request_frags.append(fragment[:end])
            self.requestInfo = parseHTTPRequest("".join(self.request_frags))
        else:
            self.request_frags.append(fragment)


    def build_response(self, fragment):
        end = fragment.find("\r\n\r\n")
        if end > -1:
            self.response_frags.append(fragment[:end])
            self.responseInfo = parseHTTPResponse("".join(self.response_frags))
            self.write()
            self.erase()
        else:
            self.response_frags.append(fragment)


    def write(self):
        entry = " ".join(self.requestInfo + self.responseInfo) + "\n"
        httplog.write(entry)
        httplog.flush()

        
    def clear(self):
        self.request_frags = []
        self.response_frags = []
        self.requestInfo = None
        self.responseInfo = None

    def erase(self):
        streams.remove(self)


#returns (host_name, method, path, version)
def parseHTTPRequest(header):
    header = header.strip()
    lines = header.split("\r\n")
    method, path, version = lines[0].split(" ")
    fields = [tuple(line.split(": ")) for line in lines[1:]]
    fields = dict([(k.lower(), v) for (k, v) in fields])
    host_name = fields["host"] if "host" in fields else None
    return (host_name, method, path, version)


#returns (status_code, object_size)
def parseHTTPResponse(header):
    header = header.strip()
    lines = header.split("\r\n")
    status_code = lines[0].split(" ")[1]
    fields = [tuple(line.split(": ")) for line in lines[1:]]
    fields = dict([(k.lower(), v) for (k, v) in fields])
    object_size = fields["content-length"] if "content-length" in fields else -1
    return (status_code, object_size)

######

#converts an "a.b.c.d" IP address to a number
def addressToInteger(ip):
    bytes = "".join([chr(int(e)) for e in ip.split(".")])
    return struct.unpack('!L', bytes)[0]

#converts "a.b.c.d/size" to a range (tuple) of numbers
def prefixToRange(ip, size):
    num = bin(addressToInteger(ip))[2:]
    zeros = '0' * (32 - len(num))
    num = zeros + num
    prefix = num[:size]
    minSuffix = '0' * (32 - size)
    maxSuffix = '1' * (32 - size)
    return (int(prefix + minSuffix, 2), int(prefix + maxSuffix, 2))

#returns True if num if within range, inclusive
def inRange(num, range):
    return num >= range[0] and num <= range[1]


#returns the n least significant bits of num
def lsb(num, n):
    return num % pow(2, n)

#returns the (#bits - n) most significant bits of num
def msb(num, n):
    return num // pow(2, n)
